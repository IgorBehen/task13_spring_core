package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanValidator;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class BeanConfigBPP implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if (bean instanceof BeanValidator) {
            ((BeanValidator) bean).validate();
        }
        return bean;
    }
}
