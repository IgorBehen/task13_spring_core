package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("my.properties")
public class BeanConfigC {
    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private int value;
    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC() {
        return new BeanC(name, value);
    }
}

