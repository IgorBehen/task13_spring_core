package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanA;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({BeanConfigB.class})
public class BeanConfigA {
    @Bean
    public BeanA getBeanA() {
        BeanA bean = new BeanA();
        bean.setName("This is a BeanA");
        bean.setValue(1);
        return bean;
    }
}

