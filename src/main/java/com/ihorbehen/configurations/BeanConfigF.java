package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanF;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BeanConfigF {

    @Value("${beanF.name}")
    private String name;
    @Value("${beanF.value}")
    private int value;
    @Bean(name = "beanE")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF(name, value);
    }
}
