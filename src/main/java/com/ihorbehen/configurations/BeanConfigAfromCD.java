package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanA;
import com.ihorbehen.beans.BeanC;
import com.ihorbehen.beans.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigAfromCD {
    @Bean
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean
    public BeanA getBeanA(BeanC getBeanC, BeanD getBeanD) {
        System.out.println("From BeanA -> : " + getBeanC);
        System.out.println("From BeanA -> : " + getBeanD);
        return new BeanA(getBeanC, getBeanD);
    }
}


