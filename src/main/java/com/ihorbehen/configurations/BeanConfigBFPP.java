package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanA;
import com.ihorbehen.beans.BeanB;
import com.ihorbehen.beans.BeanBFPP;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigBFPP {

    @Bean(initMethod = "init")
    @Qualifier("beanBFPP")
    public BeanBFPP getMyBeanBFPP() {
        return new BeanBFPP();
    }

    @Bean(initMethod = "close")
    public BeanB getBeanB(BeanBFPP getMyBeanBFPP) {
        System.out.println("From BeanB -> : " + getMyBeanBFPP);
        return new BeanB(getMyBeanBFPP);
    }
}
