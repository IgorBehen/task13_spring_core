package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanA;
import com.ihorbehen.beans.BeanE;
import org.springframework.context.annotation.Bean;

public class BeanConfigE {

    @Bean
    public BeanE getBeanE(BeanA myBeanA) {
        System.out.println("From BeanE �0 -> : " + myBeanA);
        return new BeanE(myBeanA);
    }

    //�2
//    @Bean
//    @Autowired
//    public BeanE getBeanE1(@Qualifier("beanA") BeanA myBeanA) {
//        System.out.println("From BeanE -> : " + myBeanA);
//        return new BeanE(myBeanA);
//    }
}
