package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanB;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("my.properties")
public class BeanConfigB {
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;
    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    @Qualifier("myBean")
    public BeanB getBeanB() {
        return new BeanB(name, value);
    }
}


