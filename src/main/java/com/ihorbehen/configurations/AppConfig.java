package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanB;
import com.ihorbehen.beans.BeanC;
import com.ihorbehen.beans.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class AppConfig {
    @Bean("beanC")
    @DependsOn(value = {"beanD", "beanB"})
    public BeanC getBeanC() {
        BeanC bean = new BeanC();
        bean.setName("AAA");
        bean.setValue(321);
        return bean;
    }
    @Bean("beanD")
    public BeanD getBeanD() {
        System.out.println("BeanD Initialized");
        return new BeanD();
    }
    @Bean("beanB")
    public BeanB getBeanB() {
        System.out.println("BeanB Initialized");
        return new BeanB();
    }
}




