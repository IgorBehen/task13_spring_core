package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanA;
import com.ihorbehen.beans.BeanB;
import com.ihorbehen.beans.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigAfromBD {

    @Bean
    public BeanB getMyBeanB1() {
        return new BeanB();
    }

    @Bean
    public BeanD getMyBeanD() {
        return new BeanD();
    }

    @Bean
    public BeanA getBeanA(BeanB myBeanB1, BeanD myBeanD) {
        System.out.println("From BeanA -> : " + myBeanB1);
        System.out.println("From BeanA -> : " + myBeanD);
        return new BeanA(myBeanB1, myBeanD);
    }
}


