package com.ihorbehen.configurations;

import com.ihorbehen.beans.BeanA;
import com.ihorbehen.beans.BeanB;
import com.ihorbehen.beans.BeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("my.properties")
public class BeanConfigAfromBC {

    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;
    @Bean
    @Qualifier("beanB")
    public BeanB getMyBeanB() {
    return new BeanB(nameB, valueB);
}
    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;
    @Bean
    @Qualifier("beanC")
    public BeanC getMyBeanC() {
        return new BeanC(nameC, valueC);
    }
    @Bean
    @Autowired
    public BeanA getBeanA(@Qualifier("beanB") BeanB myBeanB,
                          @Qualifier("beanC") BeanC myBeanC) {
        System.out.println("From BeanA -> : " + myBeanB);
        System.out.println("From BeanA -> : " + myBeanC);

        BeanA bean = new BeanA();
        bean.setName("This is a BeanA From BeanConfigAfromBC");
        bean.setValue(11);
        return bean;
    }
}

