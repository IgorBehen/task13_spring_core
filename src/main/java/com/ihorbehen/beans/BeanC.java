package com.ihorbehen.beans;

public class BeanC implements BeanValidator {
    private String name;
    private int value;
    private boolean isValid;

    public BeanC() {
    }

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", isValid=" + isValid +
                '}';
    }

    public boolean isValid() {
        return isValid;
    }

    @Override
    public void validate() {
        if (name != null && value > 0) {
            isValid = true;
        }
    }

    private void init() {
        System.out.println("BeanC Initialized");
    }

    private void destroy() {
        System.out.println("BeanC Destroyed");
    }
}
