package com.ihorbehen.beans;

import org.springframework.beans.factory.DisposableBean;

public class BeanD implements BeanValidator, DisposableBean {
    private String name;
    private int value;

    public BeanD() {
    }

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {

    }

    private void init() {
        System.out.println("BeanD Initialized");
    }

    public void destroy() {
        System.out.println("BeanD Destroyed");
    }
}
