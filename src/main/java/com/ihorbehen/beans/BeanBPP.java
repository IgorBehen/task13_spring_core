package com.ihorbehen.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * ��, �� �������� BeanPostProcessor
 */
public class BeanBPP implements InitializingBean, BeanPostProcessor, BeanValidator {
    private String name;
    private int value;
    private boolean isValid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }
    public boolean isValid() {
        return isValid;
    }

    @Override
    public void validate() {
        if (name != null && value > 0) {
            isValid = true;
        }
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", isValid=" + isValid +
                '}';
    }
}
