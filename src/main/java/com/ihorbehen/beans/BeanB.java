package com.ihorbehen.beans;

public class BeanB implements BeanValidator {

    private String name;
    private int value;
    private boolean isValid;
    private BeanBFPP myBeanBFPP;

    public BeanB() {
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public BeanB(BeanA myBean) {
    }

    public BeanB(BeanBFPP getMyBeanBFPP) {
        this.myBeanBFPP = getMyBeanBFPP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", isValid=" + isValid +
                '}';
    }

    public boolean isValid() {
        return isValid;
    }

    @Override
    public void validate() {
        if (name != null && value > 0) {
            isValid = true;
        }
    }

    public void init() {
        System.out.println("BeanB Initialized");
    }

    private void destroy() {
        System.out.println("BeanB Destroyed");
    }

    private void close() {
        System.out.println("From BeanConfigBFPP changing init() on close() in BeanB");
    }
}
