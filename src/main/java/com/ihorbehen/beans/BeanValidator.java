package com.ihorbehen.beans;

public interface BeanValidator {
    void validate();
}
