package com.ihorbehen.beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("beanA")
public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private int value;
    private BeanD myBeanD;
    private BeanC myBeanC;
    private boolean isValid;

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public BeanA(BeanB myBean) {
    }

    public BeanA(BeanB myBeanB, BeanD myBeanD) {
        this.myBeanD = myBeanD;
    }

    public BeanA(BeanC getBeanC, BeanD getBeanD) {
        this.myBeanC = getBeanC;
        this.myBeanD = getBeanD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isValid() {
        return isValid;
    }

    @Override
    public void validate() {
        if (name != null && value > 0) {
            isValid = true;
        }
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", myBeanD=" + myBeanD +
                ", myBeanC=" + myBeanC +
                ", isValid=" + isValid +
                '}';
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("This is InitializingBean of BeanA - afterPropertiesSet()");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("This is DisposableBean of BeanA - destroy()");
    }
}
