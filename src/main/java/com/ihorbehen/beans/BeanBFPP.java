package com.ihorbehen.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * ��, �� �������� BeanFactoryPostProcessor
 */
public class BeanBFPP implements BeanFactoryPostProcessor {
    private void init() {
        System.out.println("$$$$$$$$ BeanBFPP Initialized");
    }
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory)
            throws BeansException {
        for (String beanName : configurableListableBeanFactory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = configurableListableBeanFactory.getBeanDefinition(beanName);
            System.out.println(">>>>>>> " + "getFactoryMethodName(): " + beanDefinition.getFactoryMethodName());
             System.out.println("@@@@@@@ beanDefinition " + beanDefinition);
        }
    }
}
