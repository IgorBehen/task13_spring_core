package com.ihorbehen.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private String name;
    private int value;
    private BeanA myBeanA;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public BeanE(BeanA myBeanA) {
        this.myBeanA = myBeanA;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", myBeanA=" + myBeanA +
                '}';
    }

    @Override
    public void validate() {
    }

    @PostConstruct
    public void annoInitMethod() {
        System.out.println("inside @PostConstruct-method");
    }

    @PreDestroy
    public void annoDestroyMethod() {
        System.out.println("inside @PreDestroy-method");
    }

}
