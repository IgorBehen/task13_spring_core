package com.ihorbehen.app;

import com.ihorbehen.beans.*;
import com.ihorbehen.configurations.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigA.class, BeanConfigB.class,
                BeanConfigC.class, BeanConfigD.class, BeanConfigE.class, BeanConfigF.class);

        BeanA beanA = context.getBean(BeanA.class);
        BeanB beanB = context.getBean(BeanB.class);
        BeanC beanC = context.getBean(BeanC.class);
        BeanD beanD = context.getBean(BeanD.class);
        BeanE beanE = context.getBean(BeanE.class);
        BeanF beanF = context.getBean(BeanF.class);
        System.out.println(beanA);
        System.out.println(beanB);
        System.out.println(beanC);
        System.out.println(beanD);
        System.out.println(beanE);
        System.out.println(beanF);

        //� ���� ������������ @Configuration ����������� 3 ���� ������� ��������� ��� BeanA �� ����� �������: eanC �� BeanD (by type)
        System.out.println(" ### This is a BeanConfigAfromCD(by type): ");
        ApplicationContext contextAfromCD = new AnnotationConfigApplicationContext(BeanConfigAfromCD.class);
        BeanA beanAfromCD = contextAfromCD.getBean(BeanA.class);
        System.out.println(beanAfromCD);

        //� ���� ������������ @Configuration ����������� 3 ���� ������� ��������� ��� BeanA �� ����� �������: eanB �� BeanD (by type)
        System.out.println(" ### This is a BeanConfigAfromBD(by type): ");
        ApplicationContext contextAfromBD = new AnnotationConfigApplicationContext(BeanConfigAfromBD.class);
        BeanA beanAfromBD = contextAfromBD.getBean(BeanA.class);
        System.out.println(beanAfromBD);

        //� ���� ������������ @Configuration ����������� 3 ���� ������� ��������� ��� BeanA �� ����� �������: eanB �� BeanC (by @Qualifier)
        System.out.println(" ### This is a BeanConfigAfromBC(by @Qualifier): ");
        ApplicationContext contextAfromBC = new AnnotationConfigApplicationContext(BeanConfigAfromBC.class);
        BeanA beanAfromBC = contextAfromBC.getBean(BeanA.class);
        System.out.println(beanAfromBC);

        //����������� ����� ������� ��������� ����: BeanD, BeanB, BeanC
          System.out.println(" ### This is a BeanD, BeanB, BeanC ordering from AppConfig: ");
        ApplicationContext contextDBC = new AnnotationConfigApplicationContext(AppConfig.class);
        BeanC bean = contextDBC.getBean(BeanC.class);
        System.out.println(bean);


        //This is a Bean impl. BeanFactoryPostProcessor
        System.out.println(" ### This is a Bean impl. BeanFactoryPostProcessor");
        ApplicationContext contextBFPP = new AnnotationConfigApplicationContext(BeanConfigBFPP.class);
        BeanBFPP beanBFPP = contextBFPP.getBean(BeanBFPP.class);
        System.out.println(beanBFPP);

        ((AnnotationConfigApplicationContext) context).close();
        ((AnnotationConfigApplicationContext) contextAfromCD).close();
        ((AnnotationConfigApplicationContext) contextAfromBD).close();
        ((AnnotationConfigApplicationContext) contextAfromBC).close();
        ((AnnotationConfigApplicationContext) contextDBC).close();
        ((AnnotationConfigApplicationContext) contextBFPP).close();
    }
}
